
resource "aws_instance" "bastion" {
  ami                    = var.ami_id
  instance_type          = "t2.small"
  vpc_security_group_ids = var.security_group_ids
  subnet_id              = var.subnet_id
  key_name               = var.keyname
}

resource "aws_eip" "bastion" {
  instance = aws_instance.bastion.id
  vpc      = true
}

